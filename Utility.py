# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 12:38:42 2020

@author Paolo A
"""
import re
import datetime

def validacionPlaca(plate):
    match = re.search(r'^[a-z,A-Z]{3}-[0-9]{4}', plate)
    if match is None:
        print('Por favor insertar una  placa valida')
        return False 
    else:
        return True
  
def validacionHora(hour):
    # HH:MM
    match = re.search(r'^(2[0-3]|[01]?[0-9]):([0-5][0-9])$', hour)
    if match is None:
        print('Por favor ingresar una hora con el formato correcto HH:MM')
        return False 
    else:
        return True
  

def validacionFecha(date):
    # DD/MM/AAAA
    match = re.search(r'^(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d$', date) 
    result = list()
    if match is None:
        print('Por Favor ingresar una fecha valida con el formato correcto DD/MM/AAAA')
        result.append(False)
        return result
    else:
        day, month, year = date.split('/')
        try:
            diaPredict = datetime.datetime(int(year), int(month), int(day))
            result.append(True)
            result.append(diaPredict)
            return result
        except:
            print('Por Favor ingresar una fecha valida')
            result.append(False)
            return result
        return True
    
def transformDay(day):
        switcher = {
                    "0": "LUNES",
                    "1": "MARTES",
                    "2": "MIERCOLES",
                    "3": "JUEVES",
                    "4": "VIERNES",
                    "5": "SABADO",
                    "6": "DOMINGO",
                }
        return switcher.get(str(day))


