
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 18 00:10:02 2020

@author: Paolo A
"""

import unittest
import main
import datetime
import Utility

class PicoyPlacaPredictorTest(unittest.TestCase):

    def testPredictor(self):
        diaPredict = datetime.datetime(2020, 1, 17)
        self.assertEqual(main.mainTest('PTP-0630',diaPredict,'08:30'), 'No puede circular')
    
    def testPredictor2(self):
        diaPredict = datetime.datetime(2020, 1, 18)
        self.assertEqual(main.mainTest('PTP-0630',diaPredict,'08:30'), 'Puede circular')

    def testPredictor3(self):
        diaPredict = datetime.datetime(2020, 2, 5)
        self.assertEqual(main.mainTest('PTP-0635',diaPredict,'08:30'), 'No puede circular')

    def testValidacionPlaca(self):
        self.assertEqual(Utility.validacionPlaca("PTP-0636"),True)
    
    def testValidacionPlaca2(self):
        self.assertEqual(Utility.validacionPlaca("PTTP-0636"),False)

    def testValidacionHora(self):
        self.assertEqual(Utility.validacionHora("10:80"),False)

    def testValidacionHora2(self):
        self.assertEqual(Utility.validacionHora("10:30"),True)

    def testValidacionFecha1(self):
        result = list()
        diaPredict = datetime.datetime(2020, 2, 10)
        result.append(True)
        result.append(diaPredict)
        self.assertEqual(Utility.validacionFecha("10/02/2020"),result)

    def testValidacionFecha2(self):
        result = list()
        result.append(False)
        self.assertEqual(Utility.validacionFecha("30/02/2020"),result)

    def testValidacionFecha3(self):
        result = list()
        result.append(False)
        self.assertEqual(Utility.validacionFecha("14/30/2020"),result)

    

        

if __name__ == '__main__':
    unittest.main()