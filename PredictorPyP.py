# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 12:38:42 2020

@author: Paolo A
"""
import json
import datetime
from Utility import transformDay

class PredictorPyP:
    def __init__(self):
        self.dias = ""
        self.numeros = ""
        self.horarios = ""
        
    def leerRestricciones(self):
        with open('restriccionesPicoyPlaca.json') as f:
            informacion = json.load(f)
            self.dias = informacion['Dias']
            self.numeros = informacion['Numeros']
            self.horarios = informacion['Horario']
            
    def predict(self,plate,date,time):
        
        # miro si la hora ingresada esta en el rango del PyP
        # En base de las restricciones del archivo restriccionesPicoyPlaca 
        
        hour, minutes  = time.split(':')
        timePredict = datetime.time(int(hour), int(minutes))
        restriccion1 = False
        for horas in self.horarios :
            target = self.horarios.get(horas)
            hora1 = target[0]
            h1, m1  = hora1.split(':')
            hora2 = target[1]
            h2, m2  = hora2.split(':')
            target1 = datetime.time(int(h1), int(m1))
            target2 = datetime.time(int(h2), int(m2))
            if(target1 < timePredict < target2):
                restriccion1 = True
                break
        
        #Al no estar dentro del rango de horas puede circular sin importar la placa
        if(restriccion1 is False):
            return "Puede circular"

        ## obtengo el ultimo numero de la placa para encontrar el dia de PyP
        lastDigit = plate[-1]
        diaPlaca = self.numeros.get(str(lastDigit)).get('Dia')
        
        # Obtengo que dia de la semana es la fecha ingresada
        # El dia y el dia de la restriccion no puede circular
        
        diaSemana = date.weekday()
        diaTransform = transformDay(diaSemana)
        if(diaTransform == diaPlaca):
            return "No puede circular"
        else:
            return "Puede circular"
        
        
    
    
        

        