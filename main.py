# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 23:38:43 2020

@author: Paolo A
"""

import json
import datetime
import sys
from PredictorPyP import *
from Utility import *

#main con ingreso de datos del usuario
def main():
    predictor = PredictorPyP()
    predictor.leerRestricciones()

    placa = ""
    fecha = list()
    hora = ""


    while(True):
        bPlaca = False
        print("Ingresar 'Exit' para salir del programa")
        while (bPlaca is False):
            placa = input("Ingrese la placa:")
            if placa == "Exit":
                raise SystemExit
            bPlaca = validacionPlaca(placa)
            
        bHora = False
        while (bHora is False):
            hora = input("Ingrese la hora (formato HH:MM): ")
            if hora == "Exit":
                raise SystemExit
            bHora = validacionHora(hora)
            
        bfecha= False   
        while (bfecha is False):
            fechaInput = input("Ingrese la fecha (formato DD/MM/AAAA): ")
            if fecha == "Exit":
                raise SystemExit
            fecha = validacionFecha(fechaInput)
            bfecha = fecha[0]
        result = predictor.predict(placa,fecha[1],hora) 
        print(result) 
        print("----------------")

#funcion para realizar unitTest
def mainTest(placa,fecha,hora):
    predictor = PredictorPyP()
    predictor.leerRestricciones()
    result = predictor.predict(placa,fecha,hora) 
    return result 


if __name__ == '__main__':
    main()


